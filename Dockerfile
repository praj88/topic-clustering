FROM ujwal19/alpine-py3-java8

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# install sdk for alpine linux
RUN apk add --update --no-cache \
    python3-dev \
    build-base

#Install jupyter kernel gateway
#RUN pip install jupyter_kernel_gateway

#Install Curl
RUN apk add --update curl && \
    rm -rf /var/cache/apk/*

# Add the app file
ADD /app-topic-clustering /app-topic-clustering

# Get pip to download and install requirements:
RUN mv /app-topic-clustering/nltk_data /usr/share
RUN pip install -r /app-topic-clustering/requirements.txt


# Expose ports
EXPOSE 5008


# Set the default directory where CMD will execute
WORKDIR /app-topic-clustering
CMD python3 /app-topic-clustering/topic_clustering.py
