# -*- coding: utf-8 -*-
"""
Created on Sun Sep 11 06:40:08 2016

@author: Ujwal
"""
from __future__ import print_function # Python 2/3 compatibility
import numpy as np
import pandas as pd
import re
import json
import decimal
import numpy as np
from flask import Flask, url_for, request
import json


# Tf-Idf and Clustering packages
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
import lda # topic modeling -NMF & LDA



# NTLK functions
import nltk
from nltk.corpus import stopwords
from nltk import tokenize as tok
from nltk.stem.snowball import SnowballStemmer # load nltk's SnowballStemmer as variabled 'stemmer'

#dbscan library
from sklearn.cluster import DBSCAN


# --------------------- Download data from dynamodb


#inputText = pd.read_csv('/Users/prajwalshreyas/Desktop/Singularity/Topic modelling/News Data/news_sample_2.csv', encoding = "ISO-8859-1")
#processedTweets[96:]
#processedTweets = processedTweets.dropna()

# # Get a copy of all tweets and use them to further refine the words
# tweets = processedTweets['text']
# # remove urls and retweets
# tweets = tweets.str.replace(r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', '')
# tweets = tweets.str.replace(r'^RT+[\s]+(@[\w_]+:)', '')
# tweets = tweets.str.replace(r'@[\w_]+', '')


# LDA Modeling for topics and visualisation

# ----------------- Pre modelling prep

# List of stopwords
stopwords= stopwords.words('english') #import stopwords from NLTK package


# Initialise stemmer
stemmer = SnowballStemmer("english")

#flask api Initialise
app = Flask(__name__)

@app.route('/topictest')
def topictest():
    #count = redis.incr('hits')
    return 'Topic Cluster API working'

@app.route('/topicclusters', methods=['POST'])
def topicclusters():

    if request.method == 'POST':
        all_data = pd.DataFrame(request.json)
        text_data = all_data[['text','ref']]
        text_data = text_data.dropna()
        text = text_data['text']
        params = all_data[['cluster_radius','min_cluster_samples','no_iter','no_topics']]
        params = params.dropna()
        #print ((params))
        #print (type(params))
        no_topics = int(params['no_topics'][0])
        no_iter = int(params['no_iter'][0])
        cluster_radius = params['cluster_radius'][0]
        min_cluster_samples = params['min_cluster_samples'][0]
        print (text_data)
        #params = {'sessionKey': '9ebbd0b25760557393a43064a92bae539d962103', 'format': 'xml', 'platformId': 1}


        # text_data = pd.read_csv('/Users/prajwalshreyas/Desktop/Singularity/Topic modelling/News Data/news_sample_2.csv', encoding = "ISO-8859-1")
        # text = text_data['text']
        # no_topics = 3
        # no_iter = 500
        # cluster_radius = 0.5
        # min_cluster_samples = 2

        print ('Input')
        cluster_text, topics_text = get_topic_clusters(text, no_topics, no_iter, cluster_radius, min_cluster_samples)

        # Cluster output format
        cluster_out = pd.merge(text_data, cluster_text, left_index = True, right_index = True)
        cluster_out = cluster_out[['ref','Density_Cluster','Top Topic Number']]
        print ('Output')
        #Convert df t dict and the to Json
        cluster_out = cluster_out.to_dict(orient='records')
        topic_out = topics_text.to_dict(orient='records')
        cluster_out_json = json.dumps(cluster_out, ensure_ascii=False)
        topic_out_json = json.dumps(topic_out, ensure_ascii=False)

        merged_json= cluster_out_json + topic_out_json

        print ('OUT JSON')

        return merged_json



# Function for Tokenizer
def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in tok.sent_tokenize(text) for word in tok.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

def kl_similarity(doc_topics_lda):

        v= doc_topics_lda
        kl_list = []
        kl_list_lists = []

        len_matrix = len(v[0])
        kl_matrix = pd.DataFrame(np.zeros((len_matrix, len_matrix)))

        def kl(p, q):
            p = np.asarray(p, dtype=np.float)
            q = np.asarray(q, dtype=np.float)
            p_sum = np.sum(p)
            q_sum = np.sum(q)
            #print (p_sum)
            #print (q_sum)
            return np.sum(np.where(p != 0,(p-q) * np.log10(p / q), 0))

        for y in range(0,len_matrix):
            #print(y)
            doc_first = v.loc[y:y]



            for x in range(0,len_matrix):

                doc_second = v.loc[x:x]
                KL=kl(doc_second,doc_first)

                #normalize the KL divergence
                kl_norm = (1 - np.exp(-KL))
                kl_matrix[y][x] = kl_norm


        return kl_matrix

    # ---------------- LDA modelling
def get_topic_clusters(text, no_topics, no_iter, dist, min_cluster_samples):
    # text_in = pd.read_csv('/Users/prajwalshreyas/Desktop/Singularity/Topic modelling/News Data/news_sample_2.csv', encoding = "ISO-8859-1")
    # text = text_in['text']
    # no_topics = 6
    # no_iter = 500
    # dist = 0.5
    # min_cluster_samples = 2

    #initalise variables
    topic_words_df = pd.DataFrame()
    no_docs = len(text)
    n_top_words = 20   # no of words to describe the topics

    # Generate tf-idf matrix and vocabulary
    corpus = text.tolist()
    tf_vectorizer = CountVectorizer(max_df=0.9, min_df=0.00, stop_words=stopwords, tokenizer=tokenize_only) # Use tf (raw term count) features for LDA.
    tf = tf_vectorizer.fit_transform(corpus)
    vocab = tf_vectorizer.vocabulary_
    #len(vocab)
    #print(vocab.items())

    vocab_list = [(v, k) for k, v in vocab.items()]
    vocab_df = pd.DataFrame(vocab_list)
    vocab_df = vocab_df[1]
    #vocab_df.size
    #print("type(vocab): {}".format(type(vocab_tup)))
    #print("len(vocab): {}\n".format(len(vocab_tup)))
    #Fitting LDA models with tf features
    model = lda.LDA(n_topics= no_topics, n_iter=no_iter,random_state=1).fit(tf)



    # input 1
    topic_term_dists = model.topic_word_
    #print("type(topic_word): {}".format(type(topic_term_dists)))
    #print("shape: {}".format(topic_term_dists.shape))

   #Generate topic words as part of the final output data
    for i, topic_dist in enumerate(topic_term_dists):
         #print(topic_dist)
         #print(np.array(vocab_df).size)
         topic_words = np.array(vocab_df)[np.argsort(topic_dist)][:-(n_top_words+1):-1]
         #j = i+1
         #topic_words_df = topic_words_df.append(pd.DataFrame(topic_words))
         topics = ('Topic {}: {}'.format(i, ' '.join(topic_words)))
         #print (topics)
         topic_words_df.loc[i, 'topics'] = topics


    # the document topic distrubution is used as an input to calculate KL divergence i.e. simialaruty between proablity distributions
    doc_topic_dists = model.doc_topic_
    df_doc_topic_dists = pd.DataFrame(doc_topic_dists)

    #KL similarity
    kl_out = kl_similarity(df_doc_topic_dists)
    cluster_label_dbsc = DBSCAN(eps = dist, min_samples = min_cluster_samples, metric='euclidean').fit_predict(kl_out)

    result = pd.DataFrame()
    result = pd.DataFrame({'Text': text, 'Density_Cluster':cluster_label_dbsc})
    for i in range(no_docs):
        result.loc[i,'Top Topic Number'] = doc_topic_dists[i].argmax()
    #print (result)

    return result, topic_words_df

if __name__ == "__main__":
     app.run(host="0.0.0.0", debug=False, port=5008)
